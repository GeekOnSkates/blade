from blade.color import *
import blade.espeak as speech
from blade.graphics import *
from blade.loader import *

screen = None
SCREEN_WIDTH = 0
SCREEN_HEIGHT = 0
ROWS = 20
COLUMNS = 0
SCALE = 1
BLOCK_SIZE = 8
HBORDER = 0
VBORDER = 0
VERSION = "Blade 1.0.0-dev"