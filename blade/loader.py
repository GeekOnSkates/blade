import importlib, blade

def run(program):
	mod = importlib.import_module("apps." + program + ".main")
	blade.go = mod.go
	blade.key_down = mod.key_down
	blade.key_up = mod.key_up
	mod.start()