from blade import espeak, color
import pygame

class Display(object):
	"""Stores screen-related global variables"""

display = Display()

display.screen = None
display.WIDTH, display.HEIGHT = 320, 240
display.ROWS, display.COLS = 20, 40
display.HBORDER, display.VBORDER = 0, 0
display.border_color = color.BLACK
display.background_color = color.BLACK
display.text_color = color.WHITE
display.BLOCK_SIZE = 1
display.cells = []

def init():
	info = pygame.display.Info()
	display.WIDTH = info.current_w
	display.HEIGHT = info.current_h
	display.ROWS = 20
	pygame.mouse.set_visible(False)
	display.BLOCK_SIZE = int(display.HEIGHT / display.ROWS)
	display.VBORDER = int((display.HEIGHT - (display.BLOCK_SIZE * display.ROWS)) / 2)
	display.COLS = int(display.WIDTH / display.BLOCK_SIZE)
	display.HBORDER = int((display.WIDTH - (display.COLS * display.BLOCK_SIZE)) / 2)
	espeak.init()
	display.font = load_font("fonts/Consolas-64/white-on-black.bmp")  # for now

def draw():
	display.screen.fill(display.background_color)
	pygame.draw.rect(display.screen, display.border_color, (0, 0, display.WIDTH, display.VBORDER))
	pygame.draw.rect(display.screen, display.border_color, (0, display.HEIGHT - display.VBORDER, display.WIDTH, display.VBORDER))
	pygame.draw.rect(display.screen, display.border_color, (0, 0, display.HBORDER, display.HEIGHT))
	pygame.draw.rect(display.screen, display.border_color, (display.WIDTH - display.HBORDER, 0, display.HBORDER, display.HEIGHT))

def load_font(font):
    # TO-DO: Finish the "inverted" dictionary
	surface = pygame.image.load(font)
	return {
		"standard": {
			"A": surface.subsurface((0, 0, 50, 100)),
			"a": surface.subsurface((50, 0, 50, 100)),
			"B": surface.subsurface((100, 0, 50, 100)),
			"b": surface.subsurface((150, 0, 50, 100)),
			"C": surface.subsurface((200, 0, 50, 100)),
			"c": surface.subsurface((250, 0, 50, 100)),
			"D": surface.subsurface((300, 0, 50, 100)),
			"d": surface.subsurface((350, 0, 50, 100)),

			"E": surface.subsurface((0, 100, 50, 100)),
			"e": surface.subsurface((50, 100, 50, 100)),
			"F": surface.subsurface((100, 100, 50, 100)),
			"f": surface.subsurface((150, 100, 50, 100)),
			"G": surface.subsurface((200, 100, 50, 100)),
			"g": surface.subsurface((250, 100, 50, 100)),
			"H": surface.subsurface((300, 100, 50, 100)),
			"h": surface.subsurface((350, 100, 50, 100)),

			"I": surface.subsurface((0, 200, 50, 100)),
			"i": surface.subsurface((50, 200, 50, 100)),
			"J": surface.subsurface((100, 200, 50, 100)),
			"j": surface.subsurface((150, 200, 50, 100)),
			"K": surface.subsurface((200, 200, 50, 100)),
			"k": surface.subsurface((250, 200, 50, 100)),
			"L": surface.subsurface((300, 200, 50, 100)),
			"l": surface.subsurface((350, 200, 50, 100)),

			"M": surface.subsurface((0, 300, 50, 100)),
			"m": surface.subsurface((50, 300, 50, 100)),
			"N": surface.subsurface((100, 300, 50, 100)),
			"n": surface.subsurface((150, 300, 50, 100)),
			"O": surface.subsurface((200, 300, 50, 100)),
			"o": surface.subsurface((250, 300, 50, 100)),
			"P": surface.subsurface((300, 300, 50, 100)),
			"p": surface.subsurface((350, 300, 50, 100)),

			"Q": surface.subsurface((0, 400, 50, 100)),
			"q": surface.subsurface((50, 400, 50, 100)),
			"R": surface.subsurface((100, 400, 50, 100)),
			"r": surface.subsurface((150, 400, 50, 100)),
			"S": surface.subsurface((200, 400, 50, 100)),
			"s": surface.subsurface((250, 400, 50, 100)),
			"T": surface.subsurface((300, 400, 50, 100)),
			"t": surface.subsurface((350, 400, 50, 100)),

			"U": surface.subsurface((0, 500, 50, 100)),
			"u": surface.subsurface((50, 500, 50, 100)),
			"V": surface.subsurface((100, 500, 50, 100)),
			"v": surface.subsurface((150, 500, 50, 100)),
			"W": surface.subsurface((200, 500, 50, 100)),
			"w": surface.subsurface((250, 500, 50, 100)),
			"X": surface.subsurface((300, 500, 50, 100)),
			"x": surface.subsurface((350, 500, 50, 100)),

			"Y": surface.subsurface((0, 600, 50, 100)),
			"y": surface.subsurface((50, 600, 50, 100)),
			"Z": surface.subsurface((100, 600, 50, 100)),
			"z": surface.subsurface((150, 600, 50, 100)),
			"1": surface.subsurface((200, 600, 50, 100)),
			"2": surface.subsurface((250, 600, 50, 100)),
			"3": surface.subsurface((300, 600, 50, 100)),
			"4": surface.subsurface((350, 600, 50, 100)),

			"5": surface.subsurface((0, 700, 50, 100)),
			"6": surface.subsurface((50, 700, 50, 100)),
			"7": surface.subsurface((100, 700, 50, 100)),
			"8": surface.subsurface((150, 700, 50, 100)),
			"9": surface.subsurface((200, 700, 50, 100)),
			"0": surface.subsurface((250, 700, 50, 100)),
			"!": surface.subsurface((300, 700, 50, 100)),
			"@": surface.subsurface((350, 700, 50, 100)),

			"#": surface.subsurface((0, 800, 50, 100)),
			"$": surface.subsurface((50, 800, 50, 100)),
			"%": surface.subsurface((100, 800, 50, 100)),
			"^": surface.subsurface((150, 800, 50, 100)),
			"&": surface.subsurface((200, 800, 50, 100)),
			"*": surface.subsurface((250, 800, 50, 100)),
			"(": surface.subsurface((300, 800, 50, 100)),
			")": surface.subsurface((350, 800, 50, 100)),

			"=": surface.subsurface((0, 900, 50, 100)),
			"+": surface.subsurface((50, 900, 50, 100)),
			"-": surface.subsurface((100, 900, 50, 100)),
			"_": surface.subsurface((150, 900, 50, 100)),
			"`": surface.subsurface((200, 900, 50, 100)),
			"~": surface.subsurface((250, 900, 50, 100)),
			"[": surface.subsurface((300, 900, 50, 100)),
			"]": surface.subsurface((350, 900, 50, 100)),

			"{": surface.subsurface((0, 1000, 50, 100)),
			"}": surface.subsurface((50, 1000, 50, 100)),
			"\\": surface.subsurface((100, 1000, 50, 100)),
			"|": surface.subsurface((150, 1000, 50, 100)),
			";": surface.subsurface((200, 1000, 50, 100)),
			":": surface.subsurface((250, 1000, 50, 100)),
			"'": surface.subsurface((300, 1000, 50, 100)),
			"\"": surface.subsurface((350, 1000, 50, 100)),

			".": surface.subsurface((0, 1100, 50, 100)),
			",": surface.subsurface((50, 1100, 50, 100)),
			"/": surface.subsurface((100, 1100, 50, 100)),
			"<": surface.subsurface((150, 1100, 50, 100)),
			">": surface.subsurface((200, 1100, 50, 100)),
			"?": surface.subsurface((250, 1100, 50, 100)),
			"block-top": surface.subsurface((300, 1100, 50, 100)),
			"block-bottom": surface.subsurface((350, 1100, 50, 100)),

			" ": surface.subsurface((0, 1200, 50, 100)),
			"cursor": surface.subsurface((50, 1200, 50, 100)),
			"heart": surface.subsurface((100, 1200, 50, 100)),
			"diamond": surface.subsurface((150, 1200, 50, 100)),
			"club": surface.subsurface((200, 1200, 50, 100)),
			"spade": surface.subsurface((250, 1200, 50, 100)),
			"note": surface.subsurface((300, 1200, 50, 100)),
			"notes": surface.subsurface((350, 1200, 50, 100)),

			"up": surface.subsurface((0, 1300, 50, 100)),
			"down": surface.subsurface((50, 1300, 50, 100)),
			"left": surface.subsurface((100, 1300, 50, 100)),
			"right": surface.subsurface((150, 1300, 50, 100)),
			"hscroll": surface.subsurface((200, 1300, 50, 100)),
			"vscroll": surface.subsurface((250, 1300, 50, 100)),
			"wall_left": surface.subsurface((300, 1300, 50, 100)),
			"wall_right": surface.subsurface((350, 1300, 50, 100)),
		},
		"inverted": {
			"A": surface.subsurface((400, 0, 50, 100)),
			"a": surface.subsurface((450, 0, 50, 100)),
			"B": surface.subsurface((500, 0, 50, 100)),
			"b": surface.subsurface((550, 0, 50, 100)),
			"C": surface.subsurface((600, 0, 50, 100)),
			"c": surface.subsurface((650, 0, 50, 100)),
			"D": surface.subsurface((700, 0, 50, 100)),
			"d": surface.subsurface((750, 0, 50, 100)),
		}
	}

def draw_char(x, y, value):
	surface = display.font["standard"][value]
	char = pygame.transform.scale(surface, (display.BLOCK_SIZE, display.BLOCK_SIZE * 2))
	rect = char.get_rect()
	rect = rect.move(display.HBORDER + (x * display.BLOCK_SIZE), display.VBORDER + (y * display.BLOCK_SIZE * 2))
	display.screen.blit(char, rect)

def draw_text(x, y, text):
	for i in range(0, len(text)):
		draw_char(x + i, y, text[i])
