import pygame, blade, os, sys
pygame.init()
blade.init()
os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

import apps.main_menu.main as test
blade.go = test.go
blade.key_down = test.key_down

blade.display.screen = pygame.display.set_mode((blade.display.WIDTH, blade.display.HEIGHT), pygame.HWSURFACE | pygame.FULLSCREEN)
blade.speech.say(blade.VERSION)

done = False
while not done:
	blade.draw()
	blade.go()
	pygame.display.flip()
	for event in pygame.event.get():
		if event.type == pygame.QUIT: done = True
		elif event.type == pygame.KEYDOWN:
			blade.key_down(event.key)

blade.speech.close()
pygame.quit()
