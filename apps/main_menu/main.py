import pygame, blade, importlib, os, sys

class Data(object):
	"""Container for global variables"""

data = Data()
data.index = 0

def start():
	pass

def go():
	blade.draw_text(0, 0, blade.VERSION)
	blade.draw_char(1, data.index + 2, "right")
	data.files = os.listdir("apps")
	data.files.remove("main_menu")
	for i in range(0, len(data.files)):
		blade.draw_text(3, i + 2, data.files[i])

def key_down(key):
	if key == pygame.K_ESCAPE:
		sys.exit()
	if key == pygame.K_UP:
		data.index = data.index - 1
		if data.index == -1: data.index = len(data.files) - 1
		blade.speech.say(data.files[data.index])
	elif key == pygame.K_DOWN:
		data.index = data.index + 1
		if data.index == len(data.files): data.index = 0
		blade.speech.say(data.files[data.index])
	elif key == pygame.K_RETURN:
		blade.run(data.files[data.index])

def key_up(key):
	pass