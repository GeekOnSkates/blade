import pygame, blade, importlib

class Cursor(object):
	"""container for the cursor data"""

cursor = Cursor()

def empty_line():
	line = ""
	for i in range(0, blade.display.COLS): line = line + " "
	return line

def get_cursor_character():
	cursor.blinker = cursor.blinker + 1
	if cursor.blinker <= 200:
		return "cursor"
	elif cursor.blinker == 400:
		cursor.blinker = 0
	return " "
		

def start():
	cursor.x, cursor.y = 0, 0
	cursor.lines = [empty_line()]
	cursor.blinker = 0
	cursor.line = 0
	cursor.shift_down = False

def go():
	for i in range(0, len(cursor.lines)):
		for j in range(0, blade.display.COLS):
			blade.draw_char(j, i, cursor.lines[i][j])
	blade.draw_char(cursor.x, cursor.y, get_cursor_character())

def key_down(key):
	if key == pygame.K_ESCAPE:
		blade.run("main_menu")
	elif key == pygame.K_LEFT and cursor.x > 0:
		cursor.x = cursor.x - 1
	elif key == pygame.K_RIGHT and cursor.x < blade.display.COLS:
		cursor.x = cursor.x + 1
	elif key == pygame.K_UP and cursor.y > 0:
		cursor.line = cursor.line - 1
		cursor.y = cursor.y - 1
	elif key == pygame.K_DOWN and cursor.y < len(cursor.lines) - 1:
		cursor.line = cursor.line + 1
		cursor.y = cursor.y + 1
	elif key == pygame.K_RETURN:
		if cursor.line == len(cursor.lines) - 1:
			cursor.lines.append(empty_line())
		else: cursor.lines.insert(cursor.line, empty_line())
		cursor.x = 0
		cursor.line = cursor.line + 1
		cursor.y = cursor.y + 1
	elif key == pygame.K_LSHIFT or key == pygame.K_RSHIFT:
		return
	else:
		char = str(chr(key))
		if pygame.key.get_mods() & pygame.KMOD_SHIFT: char = char.upper()
		cursor.lines[cursor.y] = cursor.lines[cursor.y][:cursor.x] + char + cursor.lines[cursor.y][cursor.x +1:]
		cursor.x = cursor.x + 1
		# print('"' + cursor.lines[cursor.y] + '"')    # Use for debugging

def key_up(key):
	pass