# The BLADE

## The problem

Accessibility on Linux is very "hit & miss"; Ubuntu is probably the best *mainstream* distro out there, but some software (even really useful stuff like web browsers, doesn't support its accessibility API. And of course, it's Linux, it's open source, so geeks like me are always trying to hack together their own solutions.  There's a distro called TalkingArch, which is great if you're familiar with Arch (which even many of us Linux guys aren't), and even then it's just a terminal.  And like most other accessible terminals, it just spews out all the output from our commands, with no way to slow down, stop, or go back and read that output (same as graphical terminal emulators in Ubuntu).  Then there was this accessible desktop called Vinux, but that's no longer maintained and had all the same problems as Ubuntu is dealing with currently.  So unfortunately, there is no cross-platform (or even cross-distro) solution.  And most people who are blind or vision impaired - myself included - aren't always prepared to invest a crapload of time and effort trying to figure out a whole new OS that may or may not be accessible (in my case that means Macs, lol - I have no idea what I'm doing on a Mac).

## My best shot at a solution

"BLADE" stands for "Basic, Lightweight, Accessible Desktop Environment".  Accessibility is the main priority here, with the second being performance.  Why?  I want this to run on the Raspberry Pi!  I want this to be a cheap, reliable, and not super-hard-to-figure-out alternative to things like Ubuntu.

### My vision (no geeky pun intended) for this system

In a word, simple.  I originally wanted this to be a text mode thing, so no GUI stuff would even be required.  But after trying that three or four different ways, I found out that the API for doing stuff in text mode is an ancient, buggy mess and there was no way around that.  But I'm not gonna get started on that soap box (lol).  But even now that I've given up that idea, I still think the visuals should be as simple as possible.  I'm not a designer, but I can do 8-bit pixel art, and I really like the sound of a retro look and feel, like the old C64 or Apple II.  So I really don't think the initial release will look (visually) anything like a modern desktop.  But it'll definitely be a step or two above the terminal - it's gonna have menus and other things to help beginners figure it out easily.

And at the same time, I plan to make this as robust as possible.  I'm gonna start with some of the more basic apps, like a file manager and text editor.  I also want to have a way to run terminal commands, then show their text output in a way that's better for screen reader users.  But if (and Lord willing, **when**) I get these basic things done, I also want to add some fun stuff!  I'm talking an audio player, games, an email and/or Twitter client, games, a chat client, and did I mention games?  I also want it to auto-run programs from USB, which is way more newbie-friendly than the whole "mounting" thing Linux usually requires.  But those are "Py (or Pi) in the sky" long-term goals.

### On the tech side

I'm planning to use OpenBox as my window manager, and PyGame to implement the UI.  I've experimented with this combo before and really liked the results.  OpenBox is a very lightweight window manager, and PyGame can do fullscreen, supports (keyboard/mouse and controllers for input, and can even play sounds (though PyGame's sound always sucked ice on my RPI).  And Python is pretty much a standard component of all Linux distros, and there's no compiling involved, so I shouldn't have many (if any) cross-distro issues.  Of course if I wanted this to work on Windows, I would need to use a different set of speech engines, but I don't plan on doing that anytime soon.

## License

I believe in saying what you mean and meaning what you say.  I'd say Legalese is as clear as mud, but mud is crystal compared to the obscure garbage most licenses spew.  I speak two languages and majored in English, and that crap is pretty much encrypted.  So here are the terms of my license, at least for now:

1. This technology is built on other technology.  Respect the licenses of Linux, Python, PyGame, and eSpeak.

2. The BLADE is open source.  Copy it, fork it, do whatever you want to it.  It would be nice if you could mention my name (the Geek on Skates) in any credits or documentation.  But it isn't required.

3. Use at your own risk.  Don't blame me if the software doesn't work, breaks something, or causes any other problems.

## Contributing

At this point I'm just getting started, and I'd rather fly solo for version 1.0.  But once the basic stuff is done, I would totally LOVE if you or some programmer you know wants to contribute!  If you're interested, go to my website, http://www.geekonskates.com, and fill out my contact form.  If you made it this far down, thanks!
